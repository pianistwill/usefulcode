@echo off
rem --------------------------------------------------------------------------------
rem Reference site: http://www.microsoft.com/resources/documentation/windows/xp/all/proddocs/en-us/batch.mspx?mfr=true
rem
rem CONVERT_MP4.BAT copies any number of files to a directory.
rem The command uses the following syntax:
rem convert_mp4 file1 file2 ....
rem
rem Simply drag and drop any number of wmv files that you want to convert to this batch file.
rem
rem --------------------------------------------------------------------------------


rem --------------------------------------------------------------------------------
rem Let's first change to the directory containing the batch file and tools.
rem --------------------------------------------------------------------------------
cd "%~d0%~p0%
rem @echo Executing in: %cd%



rem --------------------------------------------------------------------------------
rem This is the main loop
rem --------------------------------------------------------------------------------
:dostuff
if [%1]==[] goto end
rem if /i not "%~x1"==".wmv" goto skip

@echo ================================================================================
@echo == Converting "%~f1"  to  "%~d1%~p1%~n1.mp4"
@echo ================================================================================

rem Conversion code goes here!

"mp4tools\ffmpeg.exe" -i "%~f1" -vn -sample_fmt s16 -ac 2 -f wav temp001.wav
"mp4tools\x264.exe" --level 4 --profile high --bitrate 1024 --keyint 30 -o temp002.mp4 "%~f1"
"mp4tools\neroAacEnc.exe" -cbr 128000 -he -if temp001.wav -of temp003.aac
"mp4tools\MP4Box.exe" -add temp002.mp4 -add temp003.aac "%~d1%~p1%~n1.mp4"
del temp001.wav temp002.mp4 temp003.aac

goto finished

:skip
@echo File does not have .wmv extension! ("%~f1")
goto finished


:finished
shift
goto dostuff


rem --------------------------------------------------------------------------------
rem We are done
rem --------------------------------------------------------------------------------
:end
@echo Finished processing all files.
pause
