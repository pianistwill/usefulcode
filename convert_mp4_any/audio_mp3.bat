@echo off
rem --------------------------------------------------------------------------------
rem Reference site: http://www.microsoft.com/resources/documentation/windows/xp/all/proddocs/en-us/batch.mspx?mfr=true
rem
rem AUDIO_MP3.BAT extracts audio from any number of video files.
rem The command uses the following syntax:
rem convert_mp4 file1 file2 ....
rem
rem Simply drag and drop any number of wmv files that you want to convert to this batch file.
rem
rem --------------------------------------------------------------------------------


rem --------------------------------------------------------------------------------
rem Let's first change to the directory containing the batch file and tools.
rem --------------------------------------------------------------------------------
cd "%~d0%~p0%
rem @echo Executing in: %cd%



rem --------------------------------------------------------------------------------
rem This is the main loop
rem --------------------------------------------------------------------------------
:dostuff
if [%1]==[] goto end
rem if /i not "%~x1"==".wmv" goto skip

@echo ================================================================================
@echo == Converting "%~f1"  to  "%~d1%~p1%~n1.mp3"
@echo ================================================================================

rem Conversion code goes here!

"mp4tools\ffmpeg.exe" -i "%~f1" -vn -sample_fmt s16 -ac 2 -f wav temp001.wav
"mp4tools\lame.exe" -V2 temp001.wav "%~d1%~p1%~n1.mp3"
del temp001.wav

goto finished

:skip
@echo File does not have .wmv extension! ("%~f1")
goto finished


:finished
shift
goto dostuff


rem --------------------------------------------------------------------------------
rem We are done
rem --------------------------------------------------------------------------------
:end
@echo Finished processing all files.
pause
