@echo off
rem --------------------------------------------------------------------------------
rem Reference site: http://www.microsoft.com/resources/documentation/windows/xp/all/proddocs/en-us/batch.mspx?mfr=true
rem
rem CONVERT_MP4.BAT copies any number of files to a directory.
rem The command uses the following syntax:
rem convert_mp4 file1 file2 ....
rem
rem Simply drag and drop any number of wmv files that you want to convert to this batch file.
rem
rem --------------------------------------------------------------------------------


rem --------------------------------------------------------------------------------
rem Let's first change to the directory containing the batch file and tools.
rem --------------------------------------------------------------------------------
cd "%~d0%~p0%
rem @echo Executing in: %cd%



rem --------------------------------------------------------------------------------
rem This is the main loop
rem --------------------------------------------------------------------------------
:dostuff
if [%1]==[] goto end

@echo ================================================================================
@echo == Converting "%~f1"  to  "%~d1%~p1%~n1.mp4"
@echo ================================================================================

rem Conversion code goes here!
rem 크기가 홀수인 경우 변환이 되지 않을때 아래 --vf crop:0,0,0,0 에 설정을 할 수 있다.
rem --> 둘다가 짝수일때: --vf crop:0,0,0,0
rem --> 가로가 홀수일때: --vf crop:0,0,1,0
rem --> 세로가 홀수일때: --vf crop:0,0,0,1
rem --> 둘다가 홀수일때: --vf crop:0,0,1,1
"mp4tools\x264.exe" --vf crop:0,0,0,0 --fps 20 --level 4 --profile high --bitrate 1024 --keyint 30 -o temp002.mp4 "%~f1"

goto finished

:finished
shift
goto dostuff


rem --------------------------------------------------------------------------------
rem We are done
rem --------------------------------------------------------------------------------
:end
@echo Finished processing all files.
pause
