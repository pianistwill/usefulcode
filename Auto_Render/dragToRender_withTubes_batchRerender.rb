require 'pathname'
require 'fileutils'

outPrefix = "**"

scriptDir = File.expand_path(File.dirname(__FILE__))
Dir.chdir scriptDir
renderParamsDir = "#{scriptDir}/Parameters"
renderScratchDir = "#{scriptDir}/Scratch"
templateSceneFile = "#{scriptDir}/flowshape_render_batch_rerender.sc"

def relativeToDir(input, ref_dir)
    ref_path = Pathname.new(ref_dir)
    result = Pathname.new(File.expand_path(input))
    return result.relative_path_from(ref_path).to_s
end

def extractFileName(input)
    return Pathname.new(input).basename.to_s
end

def findUniqueFile(prefix, suffix)
    if (not File.exists?(prefix + suffix))
        return prefix + suffix
    end
    
    increment = 1
    while (File.exists?(prefix + "_#{"%04d" % increment}" + suffix))
        increment = increment + 1;
    end
    
    return prefix + "_#{"%04d" % increment}" + suffix
end

puts "================================================================================"
puts "Current Dir is: #{Dir.pwd}"
puts "================================================================================"

highQuality = false

ARGV.each do|a|
    if not a.match(/\.obj$/)
        if (a.match(/high/))
            highQuality = true
        end
        next
    end

    argPath = relativeToDir(a, scriptDir)

    filePrefix = ""
    if a.match(/_solved\.obj$/)
        filePrefix = a[0..-12];
    elsif a.match(/_mesh\.obj$/)
        filePrefix = a[0..-10];
    else
        filePrefix = a[0..-5];
    end
    fileOnlyPrefix = extractFileName(filePrefix)
    
    # Remove normals & shared vertices
    argPathRender = relativeToDir(renderScratchDir + "/" + fileOnlyPrefix + "_render.obj", scriptDir)
    system("\"#{scriptDir}/../MeshToObj/mesh_filter\" \"#{argPath}\" -nonormal \"#{argPathRender}\"")
    
    tubesFileName = filePrefix + "_tubes.obj"
    configFileName = renderScratchDir + "/" + fileOnlyPrefix + "_render.sc"
    transformationsFileName = renderParamsDir + "/" + fileOnlyPrefix + "_transformations.txt"
    tubesPath = relativeToDir(tubesFileName, scriptDir)
    configPath = relativeToDir(configFileName, scriptDir)
    tubesPath = relativeToDir(tubesFileName, scriptDir)
    transformationsPath = relativeToDir(transformationsFileName, scriptDir)

    # Check transformations file
    tfArray = ["0.0 0.0 0.0 0.0 0.0 0.0 1.0"]
    if (File.exists?(transformationsPath))
        file = File.open(transformationsPath, "r")
        tfArray = file.read.split("\n")
        file.close
    end
    
    tfArray = tfArray.uniq
    
    puts "================================================================================"
    puts "Rendering file: #{argPathRender} #{tubesPath}"
    puts "================================================================================"
    
    tfCount = 0
    tfArray.each { |tf|
        tfComp = tf.split(" ")
        tfString = "    transform {
        scaleu #{tfComp[6]}
        rotatex #{tfComp[3]}
        rotatey #{tfComp[4]}
        rotatez #{tfComp[5]}
        translate #{tfComp[0]} #{tfComp[1]} #{tfComp[2]}
    }"
        
        if (tfComp.length > 7)
            eyeString = "   eye #{tfComp[7]} #{tfComp[8]} #{tfComp[9]}"
        else
            eyeString = ""
        end
        
        outPNGFileName = findUniqueFile(filePrefix, ".png")
        outPNGPath = relativeToDir(outPNGFileName, scriptDir)
        FileUtils.touch(outPNGPath)
        
        begin
            file = File.open(configPath, "w")
        rescue IOError => e
            puts e
            next
        end
        
        File.open(templateSceneFile, "r").each_line do |line|
            if line.match(/filename FACE_MESH_OBJ/)
                file.write("filename #{argPathRender}\n")
            elsif line.match(/filename TUBES_MESH_OBJ/)
                file.write("filename #{tubesPath}\n")
            elsif line.match(/type file-mesh/)
                file.write(tfString + "\n")
                file.write(line)
            elsif line.match(/eye/)
                if (eyeString.length > 0)
                    file.write(eyeString + "\n")
                else
                    file.write(line)
                end
            elsif line.match(/resolution/)
                if (highQuality)
                    file.write("resolution 1920 1080")
                else
                    file.write(line)
                end
            else
                file.write(line)
            end
        end
        
        file.close unless file == nil
        
        # Execute rendering
        system("java -jar sunflow.jar -nogui -o #{outPNGPath} #{configPath}")
        
        tfCount = tfCount + 1
    }
end
