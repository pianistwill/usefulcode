require 'pathname'
require 'fileutils'

def relativeToDir(input, ref_dir)
    ref_path = Pathname.new(ref_dir)
    result = Pathname.new(File.expand_path(input))
    return result.relative_path_from(ref_path).to_s
end

def findUniqueFile(prefix, suffix)
    if (not File.exists?(prefix + suffix))
        return prefix + suffix
    end
    
    increment = 1
    while (File.exists?(prefix + "_#{"%04d" % increment}" + suffix))
        increment = increment + 1;
    end
    
    return prefix + "_#{"%04d" % increment}" + suffix
end



puts "================================================================================"
puts "Checking arguments"
puts "================================================================================"

scriptDir = File.expand_path(File.dirname(__FILE__))
Dir.chdir scriptDir
highQuality = false
openPNGFile = false
allPrefix = ""
allObjects = []
ARGV.each do|a|
    if not a.match(/\.obj$/)
        if (a.match(/high/))
            highQuality = true
        end
        if (a.match(/openpng/))
            openPNGFile = true
        end
        next
    end

    argPath = relativeToDir(a, scriptDir)

    filePrefix = ""
    if a.match(/_solved\.obj$/)
        filePrefix = a[0..-12];
    elsif a.match(/_mesh\.obj$/)
        filePrefix = a[0..-10];
    else
        filePrefix = a[0..-5];
    end
    
    tubesPath = relativeToDir(filePrefix + "_tubes.obj", scriptDir)
    
    if (File.exists?(argPath) or File.exists?(tubesPath))
        # Remove normals & shared vertices
        facesPath = relativeToDir(filePrefix + "_render.obj", scriptDir)
        system("\"#{scriptDir}/../MeshToObj/mesh_filter.exe\" \"#{argPath}\" -nonormal \"#{facesPath}\"")
        
        allObjects.push({"prefix" => filePrefix, "face" => facesPath, "tube" => tubesPath})

        # Concatenate all file prefixes to get combined transformation file name
        if (allPrefix.empty?)
            allPrefix = filePrefix
        else
            allPrefix = allPrefix + "_" + File.basename(filePrefix)
        end
    end
end



puts "================================================================================"
puts "Parsing scene file #{scriptDir}/flowshape_render.sc"
puts "================================================================================"

sceneElements = []
curElementType = ""
curElement = ""
eyeElement = ""
faceElement = ""
tubeElement = ""
braceLevel = 0
File.open("#{scriptDir}/flowshape_render.sc", "r").each_line do |line|
    # Count the number of increase / decrease of brackets
    startLevel = braceLevel
    if (line.match(/\{/) or line.match(/\}/))
        line.each_char { |ch|
            if (ch == '{')
                braceLevel = braceLevel + 1
            elsif (ch == '}')
                braceLevel = braceLevel - 1
            end
        }
    end
    
    # Push elements based on start / end of section
    if (startLevel == 0 and braceLevel != 0)
        curElement = line
        curElementType = ""
    elsif (startLevel != 0 and braceLevel == 0)
        curElement = curElement + line

        if (curElementType.empty?)
            sceneElements.push(curElement)
        elsif (curElementType == "face")
            faceElement = curElement
        elsif (curElementType == "tube")
            tubeElement = curElement
        elsif (curElementType == "eye")
            eyeElement = curElement
        end
    else
        if (highQuality and line.match(/resolution/))
            line = "resolution 1920 1080\n"
        end
        
        if line.match(/filename FACE_MESH_OBJ$/)
            curElementType = "face"
        elsif line.match(/filename TUBES_MESH_OBJ$/)
            curElementType = "tube"
        elsif line.match(/eye/)
            curElementType = "eye"
        end

        curElement = curElement + line
    end
end


# Check transformations file
transformationsPath = relativeToDir(allPrefix + "_transformations.txt", scriptDir)
tfArray = []
if (File.exists?(transformationsPath))
    file = File.open(transformationsPath, "r")
    tfArray = file.read.split("\n")
    file.close
end
if ((tfArray.length == 0) or (tfArray.length % allObjects.length != 0))
    allObjects.length.times { |i|
        tfArray.push("0.0 0.0 0.0 0.0 0.0 0.0 1.0")
    }
end

totalNumRenders = tfArray.length / allObjects.length
(totalNumRenders).times { |i|
    # This is for rendering only the last one
    if (i < totalNumRenders-1)
        next
    end
    
    tfComp = tfArray[allObjects.length * i].split(" ")
    if (tfComp.length > 7)
        eyeString = "   eye #{tfComp[7]} #{tfComp[8]} #{tfComp[9]}"
    else
        eyeString = ""
    end

    # Write out config file
    configPath = relativeToDir(findUniqueFile(allPrefix + "_render", ".sc"), scriptDir)
    begin
        file = File.open(configPath, "w")
    rescue IOError => e
        puts e
        next
    end
    
    sceneElements.each { |elt|
        file.write(elt)
    }
    
    eyeElement.each_line { |line|
        if ((eyeString.length > 0) and line.match(/eye/))
            file.write(eyeString + "\n")
        else
            file.write(line)
        end
    }
    
    allObjects.length.times { |j|
        tfComp = tfArray[allObjects.length * i + j].split(" ")
        tfString = "    transform {
        scaleu #{tfComp[6]}
        rotatex #{tfComp[3]}
        rotatey #{tfComp[4]}
        rotatez #{tfComp[5]}
        translate #{tfComp[0]} #{tfComp[1]} #{tfComp[2]}
    }"

        if (File.exists?(allObjects[j]["face"]))
            faceElement.each_line { |line|
                if line.match(/filename FACE_MESH_OBJ$/)
                    file.write("    filename #{allObjects[j]["face"]}\n")
                elsif line.match(/type file-mesh$/)
                    file.write(tfString + "\n")
                    file.write(line)
                elsif line.match(/name/)
                    file.write("    name Tree#{2*j+0}\n")
                else
                    file.write(line)
                end
            }
        end
        
        if (File.exists?(allObjects[j]["tube"]))
            tubeElement.each_line { |line|
                if line.match(/filename TUBES_MESH_OBJ$/)
                    file.write("    filename #{allObjects[j]["tube"]}\n")
                elsif line.match(/type file-mesh$/)
                    file.write(tfString + "\n")
                    file.write(line)
                elsif line.match(/name/)
                    file.write("    name Tree#{2*j+1}\n")
                #elsif (line.match(/shader simple_blue/) and (j % 2 == 0))
                #    file.write("    shader simple_red\n")
                else
                    file.write(line)
                end
            }
        end
    }
    
    file.close unless file == nil
    
    puts "================================================================================"
    puts "Rendering number #{i}: #{configPath}"
    puts "================================================================================"
    outPNGPath = relativeToDir(findUniqueFile(allPrefix, ".png"), scriptDir)
    FileUtils.touch(outPNGPath)
    system("java -jar sunflow.jar -nogui -o #{outPNGPath} #{configPath}")
    
    if (openPNGFile)
        openFile = "\"#{scriptDir}\\#{outPNGPath}\""
        openFile.gsub!("/", "\\")
        execString = 'start "" "C:\Program Files (x86)\IrfanView\i_view32.exe" ' + openFile
        system(execString)
    end
}
