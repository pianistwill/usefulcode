require 'pathname'

outPrefix = "**"

scriptDir = File.expand_path(File.dirname(__FILE__))
Dir.chdir scriptDir
puts "================================================================================"
puts "Current Dir is: #{Dir.pwd}"
puts "================================================================================"

ARGV.each do|a|
    if not a.match(/\.obj$/)
        next
    end

    scriptPath = Pathname.new(scriptDir)
    aPath = Pathname.new(File.expand_path(a))
    argPath = aPath.relative_path_from(scriptPath).to_s

    puts "================================================================================"
    puts "Rendering file: #{argPath}"
    puts "================================================================================"
    
    begin
        file = File.open("config.txt", "w")
    rescue IOError => e
        puts e
        next
    end
    
    File.open("config_SAMPLE.txt", "r").each_line do |line|
        if line.match(/^FILENAME/)
            file.write("FILENAME #{argPath}\n")
        else
            file.write(line)
        end
    end
    
    file.close unless file == nil
    
    # Execute rendering
    system("auto_run.bat")
end

