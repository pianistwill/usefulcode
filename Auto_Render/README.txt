Use a renderer called "SunFlow" to do the blue renders.
SunFlow is a global illumination renderer written in Java.
It has as input a scene configuration file (extension ".sc") to perform the renderings.
Using this software requires Java and Ruby to be installed on the machine.
It will work with both Windows and Linux (request me for linux version).
In addition, if you have IrfanView installed it will show the image automatically after the rendering is finished.

000_Scripts directory contains the scripts needed to process the renderings.
A Maya script to export (Maya_RenderButton.txt)
A Ruby script to execute rendering (Auto_Render\dragToRender_withTubes.rb)

First open the Shelf Editor in Maya to create a new shelf entry ("SunFlow Render").
Copy the script from Maya_RenderButton.txt into the command for that shelf entry.
There are two lines to change in this script:
(1) The path specified by: "string $pathPrefix" --> Change this to be the directory containing the models (*.obj).
(2) The path specified by "system" at the end: --> Modify to be the path to ruby and the path to the ruby script (dragToRender_withTubes.rb).

Create a camera that is aiming at (0,0,0) with up vector fixed at (0,1,0).
The camera's angle of view should be exactly 30 degrees.
Move camera in the x = 0 plane.  The aim should always be at (0,0,0)
The camera must be named "camera1".
To get an accurate preview of the model, resize viewport to have an aspect ratio of 16:9.

The rendering script assumes that the files have a specific name.
To render a model, load in the mesh file (*.obj) into Maya.
Need to change this mesh's object name.
--> need to type in path name relative to path specified in the script
    Change name, use three underscores "___" to separate directory.
    
For example: if rendering the blender model
suppose for example that we woud like to the surface and tubes files
C:\\Users\\Will\\Desktop\\True2Form\\ComparisonExperiments_RERUN_Nov_30\\OURS\\blender_half.obj
C:\\Users\\Will\\Desktop\\True2Form\\ComparisonExperiments_RERUN_Nov_30\\OURS\\blender_half_tubes.obj

(1) Load the blender_half.obj into maya
(2) Looking at the script, it has the path prefix as "C:\\Users\\Will\\Desktop\\True2Form\\ComparisonExperiments_RERUN_Nov_30".
(3) Therefore, we change the of the blender mesh (in the Maya Outliner) as "OURS___blender_half".
    This will render exactly the files above (mesh and tubes).
    
Finally, it's time to position the object properly.
You can specify any translation and rotation, but currently the setup only supports uniform scale.
Note that the ground plane is defined to be y = -0.8 in the rendering configuration file.
Keep this in mind as you position the object.  It may also help to draw a plane in maya at y = -0.8.
Once satisfied with the placement, select the mesh you would like to render (in the outliner) and
click the shelf entry for "SunFlow Render".  This will produce the rendering as a PNG file in the directory
where the obj files reside.

