image {
    resolution 640 360
    aa 0 1
    filter triangle
}

% |persp|perspShape 
camera {
    type   pinhole
    eye  0.4 0.4 6
    target 0 0 0
    up     0 1 0
    fov    30
    aspect 1.77778
}

light {
    type sunsky
    up 0 1 0
    east 0 0 1
    sundir 1 1 1
    turbidity 4
    samples 64
}

shader {
    name default
    type shiny
    diff 0.2 0.2 0.2
    refl 0.1
}

shader {
    name simple_blue
    type diffuse
    diff { "sRGB nonlinear" 0.4745 0.8235 0.9176 }
}

shader {
    name simple_blue_darker
    type diffuse
    diff { "sRGB nonlinear" 0.2275 0.7490 0.8784 }
}

shader {
    name simple_yellow
    type diffuse
    diff { "sRGB nonlinear" 0.682 0.86667 0.50588 }
}

shader {
    name simple_green
    type diffuse
    diff { "sRGB nonlinear" 0.698 0.768 0.541 }
}

shader {
    name simple_red
    type diffuse
    diff { "sRGB nonlinear" 0.988 0.694 0.696 }
}

shader {
    name floor
    type diffuse
    diff 1 1 1
}

shader {
    name backdrop
    type diffuse
    diff 1 1 1
}

shader {
    name "mix.shader"
    type janino
    <code>
        import org.sunflow.core.ShadingState;
        import org.sunflow.image.Color;
        import org.sunflow.core.ParameterList;
        import org.sunflow.SunflowAPI;
        import org.sunflow.math.Point3;
        import org.sunflow.core.shader.*;

        DiffuseShader ds = new DiffuseShader();
        GlassShader   gs = new GlassShader();
        boolean       b1, b2, updflag = false;
        Color         color = new Color(0f,0f,0f);

        public Color getRadiance(ShadingState state)
        {
            Color rColor = new Color(0f,0f,0f);

            Point3 p = new Point3(state.getPoint()); // save point
            Color gColor = new Color( gs.getRadiance(state) );
            state.getPoint().set(p); // restore point
            
            Color dColor = new Color( ds.getRadiance(state) );
            rColor.set(Color.blend(dColor, gColor, 0.7f));

            Color cColor = new Color(1f,1f,1f);
            rColor.set(Color.blend(rColor, cColor, 0.1f));
            return rColor;
        }

        public boolean update(ParameterList pl, SunflowAPI api)
        {
            if (!updflag)
            {
                updflag = true; // one shot
                api.parameter( "diffuse", new Color(1.0f, 1.0f, 1.0f) );
                b2 = ds.update(pl, api); // update diffuse shader
                api.parameter( "color", new Color(0.4745f, 0.8235f, 0.9176f) );
                api.parameter( "eta", 1.001f );
                b1 = gs.update(pl, api); // update glass shader
                return (b1 && b2);
            }
            return true;
        }
    </code>
}

object {
    shader floor
    type plane
    p  0 -0.8 0
    p  1 -0.8 0
    p  0 -0.8 1
}

object {
    shader backdrop
    type plane
    p  0 0 -20
    p  1 0 -20
    p  0 1 -20
}

object {
    shader mix.shader
    type file-mesh
    name Tree1
    filename FACE_MESH_OBJ
    smooth_normals true
}

object {
    shader simple_blue
    type file-mesh
    name Tree2
    filename TUBES_MESH_OBJ
    smooth_normals true
}
