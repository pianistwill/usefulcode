program = 'mesh_cat.exe'
searchDir = (ARGV[0] + '\*.ply').gsub('\\','/')

puts 'Searching: ' + searchDir

Dir.glob(searchDir) do |file|
    cmd = 'call ' + program + ' ' + file + ' -o ' + File.dirname(file) + '/' + File.basename(file, '.ply') + '.obj'
    puts "Executing: " + cmd
    
    # do work on files ending in .obj in the desired directory
    system(cmd)
end
