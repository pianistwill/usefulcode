program = 'mesh_cat.exe'
searchDir = (ARGV[0] + '\*.obj').gsub('\\','/')

puts 'Searching: ' + searchDir

Dir.glob(searchDir) do |file|
    cmd = 'call ' + program + ' ' + file + ' -o ' + File.dirname(file) + '/' + File.basename(file, '.obj') + '.ply'
    # For ascii output use --> cmd = 'call ' + program + ' ' + file + ' -o a:' + File.dirname(file) + '/' + File.basename(file, '.obj') + '.ply'
    puts "Executing: " + cmd
    
    # do work on files ending in .obj in the desired directory
    system(cmd)
end
