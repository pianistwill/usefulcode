@echo off
rem --------------------------------------------------------------------------------
rem Reference site: http://www.microsoft.com/resources/documentation/windows/xp/all/proddocs/en-us/batch.mspx?mfr=true
rem
rem CONVERT_MP4.BAT copies any number of files to a directory.
rem The command uses the following syntax:
rem convert_mp4 file1 file2 ....
rem
rem Simply drag and drop any number of wmv files that you want to convert to this batch file.
rem
rem --------------------------------------------------------------------------------


rem --------------------------------------------------------------------------------
rem Let's first change to the directory containing the batch file and tools.
rem --------------------------------------------------------------------------------
cd "%~d0%~p0%
@echo Executing in: %cd%

set convertEXE=convert.exe

rem set up the inline optional extras
set fill=-bordercolor black -border 1x1 -floodfill +0+0 black -shave 1x1
rem fill=-opaque black
rem showmasks=( -clone 3,2,4 -combine -clone 0 -write x: -delete 0--1 )
rem grayscale=-separate -compose plus -background black -flatten
rem grayscale=-modulate 100,0
rem grayscale=-colorspace gray
set grayscale=-colorspace gray -normalize
set bgnd=( +clone -sparse-color voronoi 0,0,%%[pixel:p{0,0}] )
set background=black
set fzout=0
set fzin=0

rem --------------------------------------------------------------------------------
rem This is the main loop
rem --------------------------------------------------------------------------------
:dostuff
if [%1]==[] goto end

rem Conversion code goes here!
@echo ================================================================================
@echo == Automatically trimming image "%~f1"
@echo ================================================================================

%convertEXE% -trim "%~f1" "%~f1"

rem Autocrop and transparency script from: http://www.imagemagick.org/Usage/scripts/bg_removal
rem Images generated in the following are (number = parenthesis)
rem 0 = Original image
rem 1 = difference image (blue channel cleared)
rem 2 = outside mask  - definatally transparent
rem 3 = inside mask   - definatally opaque
rem 4 = edge mask     - parts between inside and outside
rem 5 = fully opaque pixels from original
rem 6 = final image combining all the above

%convertEXE% "%~f1" ( -clone 0 %bgnd% -compose Difference -composite %grayscale% -channel B -evaluate set 0 +channel ) ( -clone 1 -fill blue -fuzz %fzout%  %fill% -channel B -separate +channel )  ( -clone 1 -fill blue -fuzz %fzin%  %fill% -channel B -separate +channel -negate )  ( -clone 2,3 -negate -compose multiply -composite ) %showmasks% ( -clone 0,3 +matte -compose CopyOpacity -composite ) ( -clone 1 -channel R -separate +channel -clone 4 +matte -compose CopyOpacity -composite ( +clone -blur 0x30 +matte ) +swap -compose Over -composite +matte -normalize -clone 4 -compose multiply -composite -background %background% -alpha shape -clone 5 -compose over -composite ) -delete 0--2 "%~f1"

shift
goto dostuff


rem --------------------------------------------------------------------------------
rem We are done
rem --------------------------------------------------------------------------------
:end
@echo Finished processing all files.
