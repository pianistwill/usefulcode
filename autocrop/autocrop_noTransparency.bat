@echo off
rem --------------------------------------------------------------------------------
rem Reference site: http://www.microsoft.com/resources/documentation/windows/xp/all/proddocs/en-us/batch.mspx?mfr=true
rem
rem CONVERT_MP4.BAT copies any number of files to a directory.
rem The command uses the following syntax:
rem convert_mp4 file1 file2 ....
rem
rem Simply drag and drop any number of wmv files that you want to convert to this batch file.
rem
rem --------------------------------------------------------------------------------


rem --------------------------------------------------------------------------------
rem Let's first change to the directory containing the batch file and tools.
rem --------------------------------------------------------------------------------
cd "%~d0%~p0%
@echo Executing in: %cd%

set convertEXE=convert.exe

rem --------------------------------------------------------------------------------
rem This is the main loop
rem --------------------------------------------------------------------------------
:dostuff
if [%1]==[] goto end

rem Conversion code goes here!
@echo ================================================================================
@echo == Automatically trimming image "%~f1"
@echo ================================================================================

%convertEXE% -trim "%~f1" "%~f1"

shift
goto dostuff


rem --------------------------------------------------------------------------------
rem We are done
rem --------------------------------------------------------------------------------
:end
@echo Finished processing all files.
