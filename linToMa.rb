scriptDir = File.expand_path(File.dirname(__FILE__))

ARGV.each do|a|
    if not a.match(/\.lin$/)
        next
    end

    newFileName = a[0..-5] + ".ma"
    puts "Processing file: #{a} --> #{newFileName}"
    
    begin
        file = File.open(newFileName, "w")
    rescue IOError => e
        puts e
        next
    end

    file.write("//Maya ASCII 2012 scene\n")
	file.write("//Name: #{newFileName}\n")
	file.write("//Last modified: #{Time.now.to_s} \n")
	file.write("//Codeset: 1252\n")
	file.write("requires maya \"2012\";\n")
	file.write("currentUnit -l centimeter -a degree -t film;\n")
	file.write("fileInfo \"application\" \"maya\";\n")
	file.write("fileInfo \"product\" \"Maya 2012\";\n")
	file.write("fileInfo \"version\" \"2012 x64\";\n")
	file.write("fileInfo \"cutIdentifier\" \"001200000000-796618\";\n")
	file.write("fileInfo \"osv\" \"Microsoft Windows 7 Enterprise Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\";\n")

    vtx = Array.new
    curve_index = 0
    lastReadV = false
    File.open(a, "r").each_line do |line|
        if line.start_with?('v ')
            ptData = line.chomp
            vtx.push(ptData[1..-1])
            lastReadV = true
        else
            if (lastReadV)
                vtxSize = vtx.length
                file.write("createNode transform -n" << "\"curve#{curve_index}\";\n")
                file.write("createNode nurbsCurve -n " << "\"curveShape#{curve_index}\"" << " -p \"curve#{curve_index}\";\n")
                file.write("setAttr -k off \".v\";\n")
                file.write("setAttr \".cc\" -type \"nurbsCurve\"\n")
                file.write("1 #{vtxSize - 1} 0 no 3\n")
                file.write("#{vtxSize}")
                vtxSize.times do |j|
                    file.write(" #{j}")
                end
                file.write("\n")
                file.write("#{vtxSize}\n")
                vtx.each { |ptData| file.write("#{ptData}\n") }
                file.write(";\n")
                
                
                # Update necessary variables
                curve_index = curve_index + 1
                vtx.clear
            end
            lastReadV = false
        end
    end
    
    file.close unless file == nil
end
