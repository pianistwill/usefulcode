require 'pathname'

scriptDir = File.expand_path(File.dirname(__FILE__))
Dir.chdir scriptDir
puts "================================================================================"
puts "Current Dir is: #{Dir.pwd}"
puts "================================================================================"
convertExe = "C:\\Program Files\\7-Zip\\7z.exe"

ARGV.each do|a|
    #scriptPath = Pathname.new(scriptDir)
    #aPath = Pathname.new(File.expand_path(a))
    #argPath = aPath.relative_path_from(scriptPath).to_s
    argPath = a
    filePrefix = argPath
    outPath = filePrefix + ".7z"

    #puts "Converting file: #{argPath} --> #{outPath}"
    puts "================================================================================"
    puts "#{convertExe} a \"#{outPath}\" \"#{argPath}\""

    # Execute rendering
    system("\"#{convertExe}\" a \"#{outPath}\" \"#{argPath}\"")
end
