require 'pathname'

scriptDir = File.expand_path(File.dirname(__FILE__))
Dir.chdir scriptDir
puts "================================================================================"
puts "Current Dir is: #{Dir.pwd}"
puts "================================================================================"
convertExe = "C:\\Program Files (x86)\\Inkscape\\inkscape.exe"

ARGV.each do|a|
    if not a.match(/\.svg$/)
        next
    end

    #scriptPath = Pathname.new(scriptDir)
    #aPath = Pathname.new(File.expand_path(a))
    #argPath = aPath.relative_path_from(scriptPath).to_s
    argPath = a
    filePrefix = argPath[0..-5]
    outPath = filePrefix + ".pdf"

    #puts "Converting file: #{argPath} --> #{outPath}"
    puts "================================================================================"
    puts "#{convertExe} --export-area-drawing --export-pdf=#{outPath} #{argPath}"

    # Execute rendering
    system("\"#{convertExe}\" --export-area-drawing --export-pdf=#{outPath} #{argPath}")
end

